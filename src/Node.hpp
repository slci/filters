/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Sławomir Cielepak <slawomir.cielepak@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef NODE_HPP
#define NODE_HPP

#include <array>
#include <set>

namespace slci::filters
{

enum class NodeState
{
    idle,
    processing
};

class Processable
{
protected:
    NodeState mState{NodeState::idle};

public:
    virtual void update() = 0;

    virtual void reset()
    {
    }

    virtual ~Processable() = default;
};

template <typename Impl, size_t OutSize = 1, typename SampleT = double>
class SourceNode : public Processable
{
public:
    std::array<SampleT, OutSize> mOutputVector;

    virtual ~SourceNode() = default;

    virtual void update() override
    {
        if (mState == NodeState::idle)
        {
            mState = NodeState::processing;

            static_cast<Impl *>(this)->process();
        }
    }

    virtual void reset() override
    {
        mState = NodeState::idle;
    }

    template <size_t OutIdx, size_t InIdx, typename NodeT>
    void connectTo(NodeT &destNode)
    {
        destNode.mInputNodes.insert(this);
        std::get<InIdx>(destNode.mInputVector) = &std::get<OutIdx>(mOutputVector);
    }

protected:
    template <size_t OutIdx>
    SampleT &output()
    {
        return std::get<OutIdx>(mOutputVector);
    }
};

template <typename Impl, size_t InSize = 1, typename SampleT = double>
class SinkNode
{
public:
    std::set<Processable *> mInputNodes;
    std::array<const SampleT *, InSize> mInputVector;

    virtual ~SinkNode() = default;

    void update()
    {
        for (auto &input_node : mInputNodes)
        {
            input_node->update();
        }

        for (auto &input_node : mInputNodes)
        {
            input_node->reset();
        }

        static_cast<Impl *>(this)->process();

        // return mInputVector;
    }

protected:
    template <size_t InIdx>
    const SampleT &input() const
    {
        return *std::get<InIdx>(mInputVector);
    }
};

template <typename Impl, size_t InSize = 1, size_t OutSize = 1, typename SampleT = double>
class Node : public Processable
{
public:
    using InputVector_t = std::array<const SampleT *, InSize>;
    using OutputVector_t = std::array<SampleT, OutSize>;

    std::set<Processable *> mInputNodes;

    InputVector_t mInputVector;
    OutputVector_t mOutputVector;

    virtual ~Node() = default;

    virtual void update() override
    {
        if (mState == NodeState::idle)
        {
            mState = NodeState::processing;

            for (auto &input_node : mInputNodes)
            {
                input_node->update();
            }

            static_cast<Impl *>(this)->process();
        }
    }

    virtual void reset() override
    {
        mState = NodeState::idle;

        for (auto &input_node : mInputNodes)
        {
            input_node->reset();
        }
    }

    template <size_t OutIdx, size_t InIdx, typename NodeT>
    void connectTo(NodeT &destNode)
    {
        if ((void *)&destNode != (void *)this)
        {
            destNode.mInputNodes.insert(this);
        }

        std::get<InIdx>(destNode.mInputVector) = &std::get<OutIdx>(mOutputVector);
    }

protected:
    template <size_t InIdx>
    const SampleT &input() const
    {
        return *std::get<InIdx>(mInputVector);
    }

    template <size_t OutIdx>
    SampleT &output()
    {
        return std::get<OutIdx>(mOutputVector);
    }
};

} // namespace slci::filters

#endif /* NODE_HPP */
