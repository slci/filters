#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include <Node.hpp>

/**
 * @brief Source that returns a series: [1.0, 2.0, 3.0, 4.0, ...]
 */
struct SimpleSource : public slci::filters::SourceNode<SimpleSource, 1>
{
    void process()
    {
        output<0>() += 1.0;
    }
};

/**
 * @brief Simple filter with feedback. Calculates simple sum of inputs.
 */
struct SimpleFilter : public slci::filters::Node<SimpleFilter, 2, 1>
{
    void process()
    {
        output<0>() = input<0>() + input<1>();
    }
};

/**
 * @brief Just a convenience alias for single input sink.
 */
struct SimpleSink : public slci::filters::SinkNode<SimpleSink, 1>
{
    double &result;

    SimpleSink(double &res) : result(res) {}

    void process()
    {
        result = input<0>();
    }
};

TEST_CASE("Simple feedback filter", "[filter]")
{
    //       ,----------------,
    //       |    ________    |
    //       '-->| filter |---'
    // [src]---->|________|----->[sink]

    // Create
    double result = 0.0;
    auto src = SimpleSource();
    auto filter = SimpleFilter();
    auto sink = SimpleSink(result);

    // Assemble
    src.connectTo<0, 1>(filter);
    filter.connectTo<0, 0>(filter); // feedback
    filter.connectTo<0, 0>(sink);

    // Process
    sink.update();
    REQUIRE(result == 1.0);

    sink.update();
    REQUIRE(result == 3.0);

    sink.update();
    REQUIRE(result == 6.0);

    sink.update();
    REQUIRE(result == 10.0);

    sink.update();
    REQUIRE(result == 15.0);
}
